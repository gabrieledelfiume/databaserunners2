import pandas as pd
import os
from pathlib import Path
from neo4j import GraphDatabase

uri = "bolt://localhost:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "Tennis_DB"))


# static methods
def create_constraints1_TennisDB(tx):
    tx.run("CREATE CONSTRAINT IF NOT EXISTS FOR (p:Player) REQUIRE p.player_id IS UNIQUE")


def create_constraints2_TennisDB(tx):
    tx.run("CREATE CONSTRAINT IF NOT EXISTS FOR (m:Match) REQUIRE m.match_id IS UNIQUE")


def create_constraints4_TennisDB(tx):
    tx.run("CREATE CONSTRAINT IF NOT EXISTS FOR (c:Country) REQUIRE c.name IS UNIQUE")


def create_constraints5_TennisDB(tx):
    tx.run("CREATE CONSTRAINT IF NOT EXISTS FOR (r:Region) REQUIRE r.name IS UNIQUE")


def create_indexes1(tx):
    tx.run("CREATE INDEX player IF NOT EXISTS FOR (p:Player) ON (p.name_list)")


def create_indexes2(tx):
    tx.run("CREATE INDEX player IF NOT EXISTS FOR (m:Match) ON (m.match_id)")


def create_indexes3(tx):
    tx.run("CREATE INDEX player IF NOT EXISTS FOR (c:Country) ON (c.name)")


def create_indexes4(tx):
    tx.run("CREATE INDEX player IF NOT EXISTS FOR (t:Tourney) ON (t.tourney_name)")


def create_player(tx, player_id, name_first, name_list, hand, birthdate):
    tx.run("MERGE (:Player {player_id: $player_id, name_first: $name_first, name_list: $name_list, hand: $hand, birthdate: date($birthdate)})",
           player_id=player_id, name_first=name_first, name_list=name_list, hand=hand, birthdate=birthdate)


def create_match(tx, match_id, score, best_of, round, minutes, w_ace, l_ace):
    tx.run("MERGE (:Match {match_id: $match_id, score: $score, best_of: $best_of, round: $round, minutes: $minutes, w_ace: $w_ace, l_ace: $l_ace})",
           match_id=match_id, score=score, best_of=best_of, round=round, minutes=minutes, w_ace=w_ace, l_ace=l_ace)

def create_tourney(tx, tourney_name, tourney_date, surface):
    tx.run("MERGE (:Tourney {tourney_name: $tourney_name, tourney_date: date($tourney_date), surface: $surface})",
           tourney_name=tourney_name, tourney_date=tourney_date,surface=surface)

def connection_to_winner(tx, match_id, player_id):
    tx.run("MATCH (p:Player {player_id: $player_id}), (m:Match {match_id: $match_id}) "
           "MERGE (p)-[r:Winner]->(m)",
           match_id=match_id, player_id=player_id)


def connection_to_loser(tx, match_id, player_id):
    tx.run("MATCH (p:Player {player_id: $player_id}), (m:Match {match_id: $match_id}) "
           "MERGE (p)-[r:Loser]->(m)",
           match_id=match_id, player_id=player_id)


def connection_player_to_country(tx, player_id, country_code):
    tx.run("MATCH (p:Player {player_id: $player_id}), (c:Country {code: $code}) "
           "MERGE (p)-[:hasCountry]->(c)",
           player_id=player_id, code=country_code)


def create_country(tx, name, code, sub_region):
    tx.run("MERGE (:Country {name: $name, code: $code, sub_region: $sub_region})",
           name=name, code=code, sub_region=sub_region)


def create_region(tx, name):
    tx.run("MERGE (:Region {name: $name})",
           name=name)


def connection_to_region(tx, countryName, region):
    tx.run("MATCH (c:Country {name: $countryName}), (r:Region {name: $region}) MERGE (c)-[:partOf]->(r)",
           countryName=countryName, region=region)


def connection_tourney_to_match(tx, match_id, tourney_name, tourney_date):
    tx.run("MATCH (t:Tourney {tourney_name: $tourney_name, tourney_date: date($tourney_date)}), (m:Match {match_id: $match_id}) "
           "MERGE (m)-[r:belongsTo]->(t)",
           match_id=match_id, tourney_name=tourney_name, tourney_date=tourney_date)

def connection_tourney_to_country1(tx, country, tourney):
    tx.run("MATCH (t:Tourney {tourney_name: $tourney}), (c:Country {code: $country}) "
           "MERGE (t)-[:located]->(c)",
           country=country, tourney=tourney)


def connection_tourney_to_country2(tx, country, tourney):
    tx.run("MATCH (t:Tourney {tourney_name: $tourney}), (c:Country {name: $country}) "
           "MERGE (t)-[:located]->(c)",
           country=country, tourney=tourney)


def create_node_and_connection_to_rank(tx, player_id, date, rank, points):
    tx.run("MATCH (p:Player {player_id: $player_id}) "
           "MERGE (p)-[:hasRank {date: date($date)}]->(:Rank {rank: $rank, points: $points})",
           player_id=player_id, date=date, rank=rank, points=points)

# paths for csv files
path = str(Path(os.path.abspath(os.getcwd())).parent.absolute())
atp_players_Url = path + '/databaserunners2/datasets/atp_players.csv'
atp_matches_Url = path + '/databaserunners2/datasets/atp_matches.csv'
countries_Url = path + '\databaserunners2\datasets\Countries.csv'
players_ranking_Url = path + '\databaserunners2\datasets\players_ranking.csv'
all_tournaments_Url = path + '\databaserunners2\datasets/all_tournaments.csv'


# read csv
players = pd.read_csv(atp_players_Url, sep=',', index_col='player_id', keep_default_na=False, na_values=['_'])
atp_matches = pd.read_csv(atp_matches_Url, sep=',', index_col='matches_id', keep_default_na=False, na_values=['_'])
countries = pd.read_csv(countries_Url, sep=',', index_col='name', keep_default_na=False, na_values=['_'])
players_ranking = pd.read_csv(players_ranking_Url, sep=',', index_col='rank', keep_default_na=False, na_values=['_'])
all_tournaments = pd.read_csv(all_tournaments_Url, sep=',', index_col='tournament', keep_default_na=False, na_values=['_'])

# create a session
session = driver.session()

# set unique constraints id
session.write_transaction(create_constraints1_TennisDB)
session.write_transaction(create_constraints2_TennisDB)
session.write_transaction(create_constraints4_TennisDB)
session.write_transaction(create_constraints5_TennisDB)


# create the indexes
session.write_transaction(create_indexes1)
session.write_transaction(create_indexes2)
session.write_transaction(create_indexes3)
session.write_transaction(create_indexes4)

# populate db with players
for index, row in players.iterrows():
    player_id = index
    name_first = row["name_first"]
    name_list = row["name_list"]
    hand = row["hand"]
    s = row["birthdate"]
    if s != '':
        try:
            birthdate = s[0:4]+'-'+s[4:6]+'-'+s[6:8]
            session.write_transaction(create_player, player_id, name_first, name_list, hand, birthdate)
        except ValueError:
            session.run("MERGE (:Player {player_id: $player_id, name_first: $name_first, name_list: $name_list, hand: $hand})",
                player_id=player_id, name_first=name_first, name_list=name_list, hand=hand)
    else:
        session.run("MERGE (:Player {player_id: $player_id, name_first: $name_first, name_list: $name_list, hand: $hand})",
           player_id=player_id, name_first=name_first, name_list=name_list, hand=hand)


# populate db with matches
for index, row in atp_matches.iterrows():
    match_id = index
    score = row["score"]
    best_of = row["best_of"]
    round = row["round"]
    minutes = row["minutes"]
    w_ace = row["w_ace"]
    l_ace = row["l_ace"]
    session.write_transaction(create_match, match_id, score, best_of, round, minutes, w_ace, l_ace)


# populate db with country, region and the connection between these two nodes
for index, row in countries.iterrows():
    name = index
    code = row["code"]
    sub_region = row["sub_region"]
    region = row["region"]
    session.write_transaction(create_country, name, code, sub_region)
    session.write_transaction(create_region, region)
    session.write_transaction(connection_to_region, name, region)


# we have to connect the winner and the loser player to the matches
for index, row in atp_matches.iterrows():
    match_id = index
    winner_id = row["winner_id"]
    loser_id = row["loser_id"]
    if winner_id != '':
        winner = int(winner_id)
        session.write_transaction(connection_to_winner, match_id, winner)
    if loser_id != '':
        loser = int(loser_id)
        session.write_transaction(connection_to_loser, match_id, loser)


# we have to connect players to the respective country
for index, row in players.iterrows():
    player_id = index
    country_code = row["country"]
    session.write_transaction(connection_player_to_country, player_id, country_code)


# populate db with tourney
for index, row in atp_matches.iterrows():
    tourney_name1 = row["tourney_name"]
    tourney_name2 = str(tourney_name1)
    tourney_name3 = tourney_name2.upper()
    d = str(row["tourney_date"])
    surface = row["surface"]
    if d != '':
        try:
            tourney_date = d[0:4]+'-'+d[4:6]+'-'+d[6:8]
            session.write_transaction(create_tourney, tourney_name3, tourney_date, surface)
        except ValueError:
            session.run("MERGE (:Tourney {tourney_name: $tourney_name, surface: $surface})",
                tourney_name=tourney_name3, surface=surface)
    else:
        session.run("MERGE (:Tourney {tourney_name: $tourney_name, surface: $surface})",
            tourney_name=tourney_name3, surface=surface)

# connection from match to tourney
for index, row in atp_matches.iterrows():
    match_id = index
    d = str(row["tourney_date"])
    tourney_name1 = row["tourney_name"]
    tourney_name2 = tourney_name1.upper()
    if d != '':
        try:
            tourney_date = d[0:4]+'-'+d[4:6]+'-'+d[6:8]
            session.write_transaction(connection_tourney_to_match, match_id, tourney_name2, tourney_date)
        except ValueError:
            session.run("MATCH (t:Tourney {tourney_name: $tourney_name}), (m:Match {match_id: $match_id}) "
                        "MERGE (m)-[r:belongsTo]->(t)",
                        match_id=match_id, tourney_name=tourney_name2)
    else:
        session.run("MATCH (t:Tourney {tourney_name: $tourney_name}), (m:Match {match_id: $match_id}) "
                    "MERGE (m)-[r:belongsTo]->(t)",
                    match_id=match_id, tourney_name=tourney_name2)


# connection from tourney to country
for index, row in all_tournaments.iterrows():
    tourney_name1 = index
    tourney_name2 = str(tourney_name1)
    tourney_name3 = tourney_name2.upper()
    country = str(row["location"])
    session.write_transaction(connection_tourney_to_country1, country, tourney_name3)
    session.write_transaction(connection_tourney_to_country2, country, tourney_name3)


# create node and connection to player's rank
for index, row in players_ranking.iterrows():
    player = row["player"]
    player_id = int(player)
    s = row["ranking_date"]
    rank = index
    rowpoints = row["points"]
    points = int(rowpoints)
    date = '2022-01-03'
    session.write_transaction(create_node_and_connection_to_rank, player_id, date, rank, points)

session.close()

driver.close()
